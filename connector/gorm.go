package connector

import (
	"backend-apps/configuration"
	"backend-apps/pkg/logx"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func InitMysqlGormConnection(cfg *configuration.ConfigApp) *gorm.DB {
	connectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?%s",
		cfg.Database.Username,
		cfg.Database.Password,
		cfg.Database.HostName,
		cfg.Database.Port,
		cfg.Database.DatabaseName,
		"parseTime=true&loc=Local")

	db, err := gorm.Open(mysql.Open(connectionString), &gorm.Config{})
	if err != nil {
		logx.GetLogger().Fatal(err)
		panic(err)
	}
	fmt.Println("Database Connected")
	return db
}
