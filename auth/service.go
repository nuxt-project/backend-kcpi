package auth

import (
	"backend-apps/configuration"
	"errors"
	"github.com/dgrijalva/jwt-go"
)

type Service interface {
	GenerateToken(userID string) (string, error)
	ValidateToken(token string) (*jwt.Token, error)
}
type jwtService struct {
	cfg *configuration.ConfigApp
}

func NewService(cfg *configuration.ConfigApp) *jwtService {
	return &jwtService{cfg: cfg}
}
func (s *jwtService) GenerateToken(userID string) (string, error) {
	claim := jwt.MapClaims{}
	claim["user_id"] = userID
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	signToken, err := token.SignedString([]byte(s.cfg.App.SecretKeyJWT))
	if err != nil {
		return signToken, nil
	}
	return signToken, nil
}
func (s *jwtService) ValidateToken(encodedToken string) (*jwt.Token, error) {
	token, err := jwt.Parse(encodedToken, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, errors.New("invalid token")
		}
		return []byte(s.cfg.App.SecretKeyJWT), nil
	})
	if err != nil {
		return token, err
	}
	return token, nil
}
