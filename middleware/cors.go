package middleware

import (
	"backend-apps/configuration"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func Cors(cfg *configuration.ConfigApp) gin.HandlerFunc {

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowOrigins = cfg.Cors.AllowOrigins
	corsConfig.AllowCredentials = true
	corsConfig.AllowHeaders = []string{"*"}
	return cors.New(corsConfig)

}
