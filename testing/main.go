package main

import (
	"backend-apps/configuration"
	"backend-apps/connector"
	"backend-apps/entity"
	micagen "backend-apps/pkg/generator"
)

func main() {

	cfg, err := configuration.LoadConfig("config.yaml")
	if err != nil {
		panic("Error loading config file")
	}

	db := connector.InitMysqlGormConnection(&cfg)
	//call micagen package
	gen := micagen.Micagen{}
	//automigrate struct of customer and generate crud rest api for customer
	gen.GenerateAll(db, &entity.Stackholders{})
}
