package input

import "time"

type InputIDSlide struct {
	ID string `uri:"id" binding:"required"`
}

type SlideInput struct {
	Text       string `form:"text" binding:"required"`
	Publish    string `form:"publish"`
	CreatedOn  time.Time
	CreatedBy  string
	ModifiedOn time.Time
	ModifiedBy string
	Deleted    int
}

//Generated by Boy at 11 Oktober 2023
