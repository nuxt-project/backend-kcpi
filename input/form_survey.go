package input

type InputIDFormSurvey struct {
	ID string `uri:"id" binding:"required"`
}

type FormSurveyInput struct {
	URL string `form:"url"`
}
