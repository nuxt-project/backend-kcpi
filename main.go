package main

import (
	"backend-apps/auth"
	"backend-apps/configuration"
	"backend-apps/connector"
	"backend-apps/entity"
	"backend-apps/handler"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/middleware"
	"backend-apps/repository"
	"backend-apps/service"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func main() {

	cfg, err := configuration.LoadConfig("config.yaml")
	if err != nil {
		panic("Error loading config file")
	}

	db := connector.InitMysqlGormConnection(&cfg)
	db.AutoMigrate(&entity.SurveyKepuasan{})

	slideRepository := repository.NewSlideRepository(db)
	slideService := service.NewSlideService(slideRepository)
	slideHandler := handler.NewSlideHandler(slideService, &cfg)

	categoryRepository := repository.NewCategoryRepository(db)
	categoryService := service.NewCategoryService(categoryRepository)
	categoryHandler := handler.NewCategoryHandler(categoryService, &cfg)

	contentStaticRepository := repository.NewContentStaticRepository(db)
	contentStaticService := service.NewContentStaticService(contentStaticRepository)
	contentStaticHandler := handler.NewContentStaticHandler(contentStaticService, categoryRepository, &cfg)

	artikelRepository := repository.NewArticleRepository(db)
	artikelService := service.NewArticleService(artikelRepository)
	artikelHandler := handler.NewArticleHandler(artikelService, categoryRepository, &cfg)

	galleryRepository := repository.NewGalleryRepository(db)
	galleryService := service.NewGalleryService(galleryRepository)
	galleryHandler := handler.NewGalleryHandler(galleryService, &cfg)

	usersRepository := repository.NewUserRepository(db)
	usersService := service.NewUserService(usersRepository)
	authService := auth.NewService(&cfg)
	usersHandler := handler.NewUserHandler(usersService, authService)

	formSurveyRepository := repository.NewFormSurvetRepository(db)
	formSurveyService := service.NewFormSurveyService(formSurveyRepository)
	formSurveyHandler := handler.NewFormSurveyHandler(formSurveyService, &cfg)

	feedBackRepository := repository.NewFeedBackRepository(db)
	feedBackService := service.NewFeedBackService(feedBackRepository)
	feedBackHandler := handler.NewFeedBackHandler(feedBackService)
	istilahRepository := repository.NewIstilahRepository(db)
	istilahService := service.NewIstilahService(istilahRepository)
	istilahHandler := handler.NewIstilahHandler(istilahService)

	libraryRepository := repository.NewLibraryRepository(db)
	libraryService := service.NewLibraryService(libraryRepository)
	libraryHandler := handler.NewLibraryHandler(libraryService)

	stackholderRepository := repository.NewStackholdersRepository(db)
	stackholderService := service.NewStackholdersService(stackholderRepository)
	stackholderHandler := handler.NewStackholdersHandler(stackholderService)

	settingRepository := repository.NewSettingRepository(db)

	surveyRepository := repository.NewSurveyKepuasanRepository(db)
	surveyService := service.NewSurveyKepuasanService(surveyRepository)
	surveyHandler := handler.NewSurveyKepuasanHandler(surveyService)

	webHandler := handler.NewWebHandler(
		&cfg,
		contentStaticRepository,
		categoryRepository,
		slideRepository,
		galleryRepository,
		artikelRepository,
		formSurveyRepository,
		istilahRepository,
		libraryRepository,
		stackholderRepository,
		feedBackRepository,
		settingRepository,
	)

	router := gin.Default()
	router.Use(middleware.Cors(&cfg))
	router.Use(middleware.TimeoutMiddleware(time.Duration(cfg.App.Timeout) * time.Second))

	api := router.Group("/api/v1")
	web := router.Group("/web/v1")

	router.Static("/img", cfg.Pathfile.Image)
	router.Static("/video", cfg.Pathfile.Video)

	api.POST("/slider", authMiddleware(authService, usersService), slideHandler.CreateSlide)
	api.PUT("/slider/:id", authMiddleware(authService, usersService), slideHandler.UpdateSlide)
	api.GET("/slider", authMiddleware(authService, usersService), slideHandler.GetSlides)
	api.GET("/slider/:id", authMiddleware(authService, usersService), slideHandler.GetSlide)
	api.DELETE("/slider/:id", authMiddleware(authService, usersService), slideHandler.DeleteSlide)

	api.POST("/category", authMiddleware(authService, usersService), categoryHandler.CreateCategory)
	api.PUT("/category/:id", authMiddleware(authService, usersService), categoryHandler.UpdateCategory)
	api.GET("/category", authMiddleware(authService, usersService), categoryHandler.GetCategorys)
	api.GET("/category/:id", authMiddleware(authService, usersService), categoryHandler.GetCategory)
	api.DELETE("/category/:id", authMiddleware(authService, usersService), categoryHandler.DeleteCategory)
	api.GET("/type_category", authMiddleware(authService, usersService), categoryHandler.GetTypeCategory)

	api.POST("/content-static", authMiddleware(authService, usersService), contentStaticHandler.CreateContentStatic)
	api.PUT("/content-static/:id", authMiddleware(authService, usersService), contentStaticHandler.UpdateContentStatic)
	api.GET("/content-static", authMiddleware(authService, usersService), contentStaticHandler.GetContentStatics)
	api.GET("/content-static/:id", authMiddleware(authService, usersService), contentStaticHandler.GetContentStatic)
	api.DELETE("/content-static/:id", authMiddleware(authService, usersService), contentStaticHandler.DeleteContentStatic)

	api.POST("/artikel", authMiddleware(authService, usersService), artikelHandler.CreateArticle)
	api.PUT("/artikel/:id", authMiddleware(authService, usersService), artikelHandler.UpdateArticle)
	api.GET("/artikel", authMiddleware(authService, usersService), artikelHandler.GetArticles)
	api.GET("/artikel/:id", authMiddleware(authService, usersService), artikelHandler.GetArticle)
	api.DELETE("/artikel/:id", authMiddleware(authService, usersService), artikelHandler.DeleteArticle)

	api.POST("/upload-img", authMiddleware(authService, usersService), galleryHandler.UploadFile)
	api.POST("/gallery", authMiddleware(authService, usersService), galleryHandler.CreateGallery)
	api.PUT("/gallery/:id", authMiddleware(authService, usersService), galleryHandler.UpdateGallery)
	api.GET("/gallery", authMiddleware(authService, usersService), galleryHandler.GetGallerys)
	api.GET("/gallery/:id", authMiddleware(authService, usersService), galleryHandler.GetGallery)
	api.DELETE("/gallery/:id", authMiddleware(authService, usersService), galleryHandler.DeleteGallery)

	api.POST("/update-password", authMiddleware(authService, usersService), usersHandler.UpdatePasssword)
	api.POST("/users", usersHandler.CreateUser)
	api.PUT("/users/:id", usersHandler.UpdateUser)
	api.GET("/users", usersHandler.GetUsers)
	api.GET("/users/:id", usersHandler.GetUser)
	api.DELETE("/users/:id", usersHandler.DeleteUser)

	api.POST("/login", usersHandler.Login)
	api.GET("/current-users", authMiddleware(authService, usersService), usersHandler.GetUserToken)

	api.POST("/form-survey", authMiddleware(authService, usersService), formSurveyHandler.CreateFormSurvey)
	api.PUT("/form-survey/:id", authMiddleware(authService, usersService), formSurveyHandler.UpdateFormSurvey)
	api.GET("/form-surveys", authMiddleware(authService, usersService), formSurveyHandler.GetAll)

	api.POST("/feedback", authMiddleware(authService, usersService), feedBackHandler.CreateFeedBack)
	api.PUT("/feedback/:id", authMiddleware(authService, usersService), feedBackHandler.UpdateFeedBack)
	api.GET("/feedback", authMiddleware(authService, usersService), feedBackHandler.GetFeedBacks)
	api.GET("/feedback/:id", authMiddleware(authService, usersService), feedBackHandler.GetFeedBack)
	api.DELETE("/feedback/:id", authMiddleware(authService, usersService), feedBackHandler.DeleteFeedBack)
	api.POST("/istilah", authMiddleware(authService, usersService), istilahHandler.CreateIstilah)
	api.PUT("/istilah/:id", authMiddleware(authService, usersService), istilahHandler.UpdateIstilah)
	api.GET("/istilah", authMiddleware(authService, usersService), istilahHandler.GetIstilahs)
	api.GET("/istilah/:id", authMiddleware(authService, usersService), istilahHandler.GetIstilah)
	api.DELETE("/istilah/:id", authMiddleware(authService, usersService), istilahHandler.DeleteIstilah)

	api.POST("/library", authMiddleware(authService, usersService), libraryHandler.CreateLibrary)
	api.PUT("/library/:id", authMiddleware(authService, usersService), libraryHandler.UpdateLibrary)
	api.GET("/library", authMiddleware(authService, usersService), libraryHandler.GetLibrarys)
	api.GET("/library/:id", authMiddleware(authService, usersService), libraryHandler.GetLibrary)
	api.DELETE("/library/:id", authMiddleware(authService, usersService), libraryHandler.DeleteLibrary)

	api.POST("/stackholder", authMiddleware(authService, usersService), stackholderHandler.CreateStackholders)
	api.PUT("/stackholder/:id", authMiddleware(authService, usersService), stackholderHandler.UpdateStackholders)
	api.GET("/stackholder", authMiddleware(authService, usersService), stackholderHandler.GetStackholderss)
	api.GET("/stackholder/:id", authMiddleware(authService, usersService), stackholderHandler.GetStackholders)
	api.DELETE("/stackholder/:id", authMiddleware(authService, usersService), stackholderHandler.DeleteStackholders)

	api.GET("/survey_kepuasan", authMiddleware(authService, usersService), surveyHandler.GetSurveyKepuasans)
	//api.PUT("/setting", authMiddleware(authService, usersService), webHandler.UpdateSetting)
	//api.GET("/setting", authMiddleware(authService, usersService), webHandler.GetSettingUpdate)

	web.GET("slider", webHandler.Slider)
	web.GET("iklim", webHandler.Iklim)
	web.GET("kategoribytype", webHandler.KategoriByType)
	web.GET("artikelbykategori/:idcategory", webHandler.ArtikelByKategori)
	web.GET("fenomena", webHandler.Gallery)
	web.GET("kategori_home", webHandler.KategoriHome)
	web.GET("konten_statis/:url_slug", webHandler.KontenStatis)
	web.GET("artikel/:url_slug", webHandler.ArtikelDetail)
	web.GET("kategoribyfilter/:filter", webHandler.CategoryBy)
	web.GET("konten_statisby/:idcategory", webHandler.KontenStatisByID)
	web.GET("form_survey", webHandler.FormSurvey)
	web.POST("feedback", webHandler.CreateFeedBack)
	web.GET("istilah", webHandler.Istilah)
	web.GET("library", webHandler.Library)
	web.GET("stackholders", webHandler.Stackholder)
	web.GET("setting", webHandler.GetSetting)
	web.POST("/survey_kepuasan", surveyHandler.CreateSurveyKepuasan)

	err = router.Run(":" + cfg.App.Port)
	if err != nil {
		return
	}

}

func authMiddleware(authService auth.Service, userService service.UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")

		if !strings.Contains(authHeader, "Bearer") {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		tokenString := ""
		arrayToken := strings.Split(authHeader, " ")
		if len(arrayToken) == 2 {
			tokenString = arrayToken[1]
		}

		token, err := authService.ValidateToken(tokenString)
		if err != nil {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)

		if !ok || !token.Valid {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		userID := claim["user_id"].(string)

		userData, err := userService.UserServiceGetByID(input.InputIDUser{
			ID: userID,
		})
		if err != nil {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		c.Set("currentUser", userData)
	}
}
