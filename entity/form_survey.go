package entity

import "time"

type FormSurvey struct {
	ID         string
	URL        string
	CreatedOn  time.Time
	ModifiedOn time.Time
}
