package entity

type Setting struct {
	ID      int    `json:"id,omitempty"`
	Alamat  string `json:"alamat,omitempty"`
	Fb      string `json:"fb,omitempty"`
	Twitter string `json:"twitter,omitempty"`
	Youtube string `json:"youtube,omitempty"`
	Ig      string `json:"ig,omitempty"`
}

func (Setting) TableName() string {
	return "setting"
}
