package entity

import "time"

type Gallery struct {
	ID         string
	Media      string
	Type       string
	Publish    string
	CreatedOn  time.Time
	CreatedBy  string
	ModifiedOn time.Time
	ModifiedBy string
	Deleted    int
}
