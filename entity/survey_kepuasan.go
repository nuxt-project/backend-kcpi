package entity

import "time"

type SurveyKepuasan struct {
	ID                            int
	Email                         string
	JenisKelamin                  string
	Kesimpulan                    string
	Kunjungan                     string
	Nama                          string
	NilaiWebsite                  string
	NilaiWebsiteGangguanTeknis    string
	NilaiWebsiteInfoAktual        string
	NilaiWebsiteKemudahanNavigasi string
	NilaiWebsiteKualitasKonten    string
	Pekerjaan                     string
	Saran                         string
	TauDariInternet               bool
	TauDariLainnya                bool
	TauDariMediaSosial            bool
	TauDariTeman                  bool
	TujuanCariDataPublikasi       bool
	TujuanCariInfo                bool
	TujuanLainnya                 bool
	TujuanMelihatBerita           bool
	Usia                          string
	CreatedAt                     time.Time
}
