package entity

type Istilah struct {
	ID        int
	Istilah   string
	Definisi  string
	Referensi string
	Tahun     string
}
