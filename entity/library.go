package entity

type Library struct {
	ID       int
	Judul    string
	Penyusun string
	Link     string
}
