package entity

type TypeCategory struct {
	IDType string `json:"id_type"`
	Name   string `json:"name"`
}

func (TypeCategory) TableName() string {
	return "type_category"
}
