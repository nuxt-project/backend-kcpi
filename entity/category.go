package entity

import "time"

type Category struct {
	ID         string
	Image      string
	Category   string
	Desc       string
	Type       string
	Publish    string
	CreatedOn  time.Time
	CreatedBy  string
	ModifiedOn time.Time
	ModifiedBy string
	Deleted    int
}
