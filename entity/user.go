package entity

import "time"

type User struct {
	ID         string
	Name       string
	Username   string
	Password   string
	Level      string
	CreatedOn  time.Time
	ModifiedOn time.Time
}
