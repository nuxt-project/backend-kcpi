package entity

type FeedBack struct {
	ID      int
	Nama    string
	Email   string
	Subject string
	Message string
}
