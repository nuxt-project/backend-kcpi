package entity

import "time"

type Slide struct {
	ID         string
	Url        string
	Text       string
	Publish    string
	CreatedOn  time.Time
	CreatedBy  string
	ModifiedOn time.Time
	ModifiedBy string
	Deleted    int
}
