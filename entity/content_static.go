package entity

import "time"

type ContentStatic struct {
	ID         string
	Image      string
	Title      string
	Content    string
	IDCategory string
	UrlSlug    string
	Publish    string
	CreatedOn  time.Time
	CreatedBy  string
	ModifiedOn time.Time
	ModifiedBy string
	Deleted    int
}
