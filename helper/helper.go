package helper

import (
	"backend-apps/configuration"
	"crypto/aes"
	"crypto/cipher"
	"encoding/hex"
	"github.com/walkert/go-evp"
)

type Response struct {
	Meta Meta        `json:"meta"`
	Data interface{} `json:"data"`
}
type Meta struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
	Status  string `json:"status"`
}

type Web struct {
	Message string      `json:"message"`
	Status  int         `json:"status"`
	Error   string      `json:"error"`
	Data    interface{} `json:"data"`
}

func WebResponse(message string, status int, error string, data interface{}) Web {
	jsonResponse := Web{
		Message: message,
		Status:  status,
		Error:   error,
		Data:    data,
	}
	return jsonResponse
}

func ApiResponse(message string, code int, status string, data interface{}) Response {
	meta := Meta{
		Message: message,
		Code:    code,
		Status:  status,
	}
	jsonResponse := Response{
		Meta: meta,
		Data: data,
	}
	return jsonResponse
}
func FormatValidationError(err error) []string {
	var errors []string
	errors = append(errors, err.Error())
	return errors
}

func EncryptText(cfg *configuration.ConfigApp, textJson []byte) (string, error) {
	salt := []byte("D3v_KCp1") // hardcoded at the moment

	// Gets key and IV from raw key.
	key, iv := evp.BytesToKeyAES256CBCMD5([]byte(salt), []byte(cfg.App.SecretKeyAES))

	// Create new AES cipher block
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	cipherText := make([]byte, len(textJson))

	// Encrypt.
	encryptStream := cipher.NewCTR(block, iv)
	encryptStream.XORKeyStream(cipherText, textJson)

	ivHex := hex.EncodeToString(iv)
	encryptedDataHex := hex.EncodeToString([]byte("Salted__")) + hex.EncodeToString(salt) + hex.EncodeToString(cipherText)
	return ivHex + ":" + encryptedDataHex, nil

}
