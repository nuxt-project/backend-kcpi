package handler

import (
	"backend-apps/configuration"
	"backend-apps/entity"
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/service"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"os"
	"path/filepath"
)

type slideHandler struct {
	service service.SlideService
	cfg     *configuration.ConfigApp
}

func NewSlideHandler(service service.SlideService, cfg *configuration.ConfigApp) *slideHandler {
	return &slideHandler{service, cfg}
}
func (h *slideHandler) GetSlide(c *gin.Context) {
	var input input.InputIDSlide
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Slide", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	slideDetail, err := h.service.SlideServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Slide", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail Slide", http.StatusOK, "success", formatter.FormatSlide(slideDetail))
	c.JSON(http.StatusOK, response)
}

func (h *slideHandler) GetSlides(c *gin.Context) {
	slides, err := h.service.SlideServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get Slides", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of Slides", http.StatusOK, "success", formatter.FormatSlides(slides))
	c.JSON(http.StatusOK, response)
}

func (h *slideHandler) CreateSlide(c *gin.Context) {
	var form input.SlideInput
	err := c.ShouldBind(&form)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Slide failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	fileSlide, err := c.FormFile("file_slider")
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Slide failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	ext := filepath.Ext(fileSlide.Filename)
	newFileName := "slider-" + uuid.New().String() + ext
	err = c.SaveUploadedFile(fileSlide, fmt.Sprintf("%s/%s", h.cfg.Pathfile.Image, newFileName))
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Slide failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	currentUser := c.MustGet("currentUser").(entity.User)
	form.CreatedBy = currentUser.ID
	form.ModifiedBy = currentUser.ID
	newSlide, err := h.service.SlideServiceCreate(form, newFileName)
	if err != nil {
		response := helper.ApiResponse("Create Slide failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Create Slide", http.StatusOK, "success", formatter.FormatSlide(newSlide))
	c.JSON(http.StatusOK, response)
}
func (h *slideHandler) UpdateSlide(c *gin.Context) {
	var inputID input.InputIDSlide
	var newFileName string
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Slides", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.SlideInput
	err = c.ShouldBind(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Slide failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	fileSlide, err := c.FormFile("file_slider")
	if fileSlide != nil {
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Slide failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		ext := filepath.Ext(fileSlide.Filename)
		newFileName = "slider-" + uuid.New().String() + ext

		//delete file sebelumnya
		dataOld, err := h.service.SlideServiceGetByID(inputID)
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Slide failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		filePath := filepath.Join(h.cfg.Pathfile.Image, dataOld.Url)
		err = os.Remove(filePath)
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Slide failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		err = c.SaveUploadedFile(fileSlide, fmt.Sprintf("%s/%s", h.cfg.Pathfile.Image, newFileName))
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Slide failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
	}

	currentUser := c.MustGet("currentUser").(entity.User)
	inputData.ModifiedBy = currentUser.ID
	updatedSlide, err := h.service.SlideServiceUpdate(inputID, inputData, newFileName)
	if err != nil {
		response := helper.ApiResponse("Failed to get Slides", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Slide", http.StatusOK, "success", formatter.FormatSlide(updatedSlide))
	c.JSON(http.StatusOK, response)
}
func (h *slideHandler) DeleteSlide(c *gin.Context) {
	id := c.Param("id")
	var inputID input.InputIDSlide
	inputID.ID = id
	dataOld, err := h.service.SlideServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Slides", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.SlideServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Slides", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	filePath := filepath.Join(h.cfg.Pathfile.Image, dataOld.Url)
	err = os.Remove(filePath)
	if err != nil {
		response := helper.ApiResponse("Failed to remove file", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Slide", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
