package handler

import (
	"backend-apps/configuration"
	"backend-apps/entity"
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/service"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

type galleryHandler struct {
	service service.GalleryService
	cfg     *configuration.ConfigApp
}

func NewGalleryHandler(service service.GalleryService, cfg *configuration.ConfigApp) *galleryHandler {
	return &galleryHandler{service, cfg}
}

func (h *galleryHandler) UploadFile(c *gin.Context) {

	fileImg, err := c.FormFile("file_img")
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Upload File failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	//formatTime := time.Now().Format("20061001-150020")
	namaFileTanpaSpasi := strings.ReplaceAll(fileImg.Filename, " ", "_")
	newFileName := "img-kcpi-" + namaFileTanpaSpasi
	var pathFile string
	pathFile = h.cfg.Pathfile.Image
	err = c.SaveUploadedFile(fileImg, fmt.Sprintf("%s/%s", pathFile, newFileName))
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Upload File failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	//response := helper.ApiResponse("Successfully Upload File", http.StatusOK, "success", newFileName)
	c.JSON(http.StatusOK, newFileName)
}

func (h *galleryHandler) GetGallery(c *gin.Context) {
	var input input.InputIDGallery
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Gallery", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	galleryDetail, err := h.service.GalleryServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Gallery", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail Gallery", http.StatusOK, "success", formatter.FormatGallery(galleryDetail))
	c.JSON(http.StatusOK, response)
}

func (h *galleryHandler) GetGallerys(c *gin.Context) {
	gallerys, err := h.service.GalleryServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get Gallerys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of Gallerys", http.StatusOK, "success", formatter.FormatGallerys(gallerys))
	c.JSON(http.StatusOK, response)
}

func (h *galleryHandler) CreateGallery(c *gin.Context) {
	var input input.GalleryInput
	err := c.ShouldBind(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Gallery failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	fileImg, err := c.FormFile("file_img")
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Gallery failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	ext := filepath.Ext(fileImg.Filename)
	newFileName := "gallery-" + uuid.New().String() + ext
	var pathFile string
	if input.Type == "image" {
		pathFile = h.cfg.Pathfile.Image
	} else {
		pathFile = h.cfg.Pathfile.Video
	}
	err = c.SaveUploadedFile(fileImg, fmt.Sprintf("%s/%s", pathFile, newFileName))
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Gallery failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	currentUser := c.MustGet("currentUser").(entity.User)
	input.CreatedBy = currentUser.ID
	input.ModifiedBy = currentUser.ID

	newGallery, err := h.service.GalleryServiceCreate(input, newFileName)
	if err != nil {
		response := helper.ApiResponse("Create Gallery failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Create Gallery", http.StatusOK, "success", formatter.FormatGallery(newGallery))
	c.JSON(http.StatusOK, response)
}
func (h *galleryHandler) UpdateGallery(c *gin.Context) {
	var inputID input.InputIDGallery
	var newFileName string
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Gallerys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.GalleryInput
	err = c.ShouldBind(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Gallery failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	fileImg, err := c.FormFile("file_img")
	if fileImg != nil {
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Gallery failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		ext := filepath.Ext(fileImg.Filename)
		newFileName = "gallery-" + uuid.New().String() + ext

		//delete file sebelumnya
		dataOld, err := h.service.GalleryServiceGetByID(inputID)
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Gallery failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		var pathFile string
		if inputData.Type == "image" {
			pathFile = h.cfg.Pathfile.Image
		} else {
			pathFile = h.cfg.Pathfile.Video
		}
		filePath := filepath.Join(pathFile, dataOld.Media)
		err = os.Remove(filePath)
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Gallery failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		err = c.SaveUploadedFile(fileImg, fmt.Sprintf("%s/%s", pathFile, newFileName))
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Content Static failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
	}

	currentUser := c.MustGet("currentUser").(entity.User)
	inputData.ModifiedBy = currentUser.ID

	updatedGallery, err := h.service.GalleryServiceUpdate(inputID, inputData, newFileName)
	if err != nil {
		response := helper.ApiResponse("Failed to get Gallerys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Gallery", http.StatusOK, "success", formatter.FormatGallery(updatedGallery))
	c.JSON(http.StatusOK, response)
}
func (h *galleryHandler) DeleteGallery(c *gin.Context) {
	id := c.Param("id")
	var inputID input.InputIDGallery
	inputID.ID = id
	dataOld, err := h.service.GalleryServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Gallerys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.GalleryServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Gallerys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var pathFile string
	if dataOld.Type == "image" {
		pathFile = h.cfg.Pathfile.Image
	} else {
		pathFile = h.cfg.Pathfile.Video
	}
	filePath := filepath.Join(pathFile, dataOld.Media)
	err = os.Remove(filePath)
	if err != nil {
		response := helper.ApiResponse("Failed to remove file", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Gallery", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
