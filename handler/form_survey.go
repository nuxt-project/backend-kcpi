package handler

import (
	"backend-apps/configuration"
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

type formSurveyHandler struct {
	service service.FormSurveyService
	cfg     *configuration.ConfigApp
}

func NewFormSurveyHandler(service service.FormSurveyService, cfg *configuration.ConfigApp) *formSurveyHandler {
	return &formSurveyHandler{service, cfg}
}

func (h *formSurveyHandler) CreateFormSurvey(c *gin.Context) {
	var input input.FormSurveyInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create User failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	newForm, err := h.service.FormSurveyServiceCreate(input)
	if err != nil {
		switch err {
		case service.UsernameIsExistError:
			response := helper.ApiResponse(err.Error(), http.StatusBadRequest, "error", nil)
			c.JSON(http.StatusBadRequest, response)
			return
		default:
			response := helper.ApiResponse("Create Form Survey failed", http.StatusBadRequest, "error", nil)
			c.JSON(http.StatusBadRequest, response)
			return
		}
	}
	response := helper.ApiResponse("Successfully Create Form Survey", http.StatusOK, "success", formatter.FormatFormSurvey(newForm))
	c.JSON(http.StatusOK, response)
}

func (h *formSurveyHandler) UpdateFormSurvey(c *gin.Context) {
	var inputID input.InputIDFormSurvey
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Users", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.FormSurveyInput
	err = c.ShouldBind(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update User failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	form, err := h.service.FormSurveyServiceUpdate(inputID, inputData)
	if err != nil {
		response := helper.ApiResponse("Failed to update", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Form Survey", http.StatusOK, "success", formatter.FormatFormSurvey(form))
	c.JSON(http.StatusOK, response)
}

func (h *formSurveyHandler) GetAll(c *gin.Context) {
	forms, err := h.service.FormSurveyServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get Form Survey", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of Form Surveys", http.StatusOK, "success", formatter.FormatFormSurveys(forms))
	c.JSON(http.StatusOK, response)
}
