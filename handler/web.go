package handler

import (
	"backend-apps/configuration"
	"backend-apps/entity"
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/repository"
	"encoding/json"
	"fmt"
	"html"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/zenazn/pkcs7pad"
)

type WebHandler struct {
	configApp         *configuration.ConfigApp
	repoContentStatic repository.ContentStaticRepository
	repoCategory      repository.CategoryRepository
	repoSlider        repository.SlideRepository
	repoGallery       repository.GalleryRepository
	repoArticle       repository.ArticleRepository
	repoFormSurvey    repository.FormSurveyRepository
	repoIstilah       repository.IstilahRepository
	repoLibrary       repository.LibraryRepository
	repoStack         repository.StackholdersRepository
	repoSetting       repository.SettingRepository
	repoFeedBack      repository.FeedBackRepository
}

func NewWebHandler(
	configApp *configuration.ConfigApp,
	repoContentStatic repository.ContentStaticRepository,
	repoCategory repository.CategoryRepository,
	repoSlider repository.SlideRepository,
	repoGallery repository.GalleryRepository,
	repoArticle repository.ArticleRepository,
	repoFormSurvey repository.FormSurveyRepository,
	repoIstilah repository.IstilahRepository,
	repoLibrary repository.LibraryRepository,
	repoStack repository.StackholdersRepository,

	repoFeedBack repository.FeedBackRepository,
	repoSetting repository.SettingRepository,
) *WebHandler {
	return &WebHandler{
		configApp:         configApp,
		repoContentStatic: repoContentStatic,
		repoCategory:      repoCategory,
		repoSlider:        repoSlider,
		repoGallery:       repoGallery,
		repoArticle:       repoArticle,
		repoFormSurvey:    repoFormSurvey,
		repoIstilah:       repoIstilah,
		repoLibrary:       repoLibrary,
		repoStack:         repoStack,
		repoSetting:       repoSetting,
		repoFeedBack:      repoFeedBack,
	}
}

func (h *WebHandler) UpdateSetting(c *gin.Context) {
	type ReqSetting struct {
		Alamat  string
		Fb      string
		Twitter string
		Youtube string
		Ig      string
	}

	var input ReqSetting
	err := c.ShouldBind(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	setting := entity.Setting{
		ID:      1,
		Alamat:  input.Alamat,
		Fb:      input.Fb,
		Twitter: input.Twitter,
		Youtube: input.Youtube,
		Ig:      input.Ig,
	}

	_, err = h.repoSetting.UpdateSetting(setting)
	if err != nil {
		response := helper.ApiResponse("update failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully updated", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)

}

func (h *WebHandler) GetSetting(c *gin.Context) {
	var response helper.Web
	data, err := h.repoSetting.FindSettingbyid(1)
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}

	response = helper.WebResponse("success get data", http.StatusOK, "", data)
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}

func (h *WebHandler) GetSettingUpdate(c *gin.Context) {
	var response helper.Web
	data, err := h.repoSetting.FindSettingbyid(1)
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}

	response = helper.WebResponse("success get data", http.StatusOK, "", data)

	c.JSON(http.StatusOK, response)
}

func (h *WebHandler) Istilah(c *gin.Context) {
	var response helper.Web
	data, err := h.repoIstilah.FindAllIstilah()
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}

	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatIstilahs(data))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}

func (h *WebHandler) Library(c *gin.Context) {
	var response helper.Web
	data, err := h.repoLibrary.FindAllLibrary()
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}

	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatLibrarys(data))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}

func (h *WebHandler) Stackholder(c *gin.Context) {
	var response helper.Web
	data, err := h.repoStack.FindAllStackholders()
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}

	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatStackholderss(data))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}

func (h *WebHandler) FormSurvey(c *gin.Context) {
	var response helper.Web
	data, err := h.repoFormSurvey.Find()
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}

	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatFormSurveys(data))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}
func (h *WebHandler) Slider(c *gin.Context) {
	var response helper.Web
	data, err := h.repoSlider.FindAllSlide()
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}

	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatSlides(data))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}

func (h *WebHandler) Iklim(c *gin.Context) {

	type dataJson struct {
		Video string `json:"video"`
		Slug  string `json:"url_slug"`
		Title string `json:"title"`
		Desc  string `json:"desc"`
	}

	var response helper.Web
	data, err := h.repoContentStatic.FindByUrLSlug("tentang-perubahan-iklim")
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	jsondata := dataJson{
		Video: "https://www.youtube.com/embed/uyfgQtXkxbU?si=LVufgBAYmWW9A5Ub&amp;controls=0",
		Slug:  data.UrlSlug,
		Title: data.Title,
		Desc:  data.Content,
	}
	response = helper.WebResponse("success get data", http.StatusOK, "", jsondata)

	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}

func (h *WebHandler) KategoriByType(c *gin.Context) {
	var response helper.Web
	tipe := c.Query("tipe")
	category, err := h.repoCategory.FindCategoryByType(tipe)
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatCategorys(category))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}

func (h *WebHandler) ArtikelByKategori(c *gin.Context) {
	var response helper.Web
	id := c.Param("idcategory")
	data, err := h.repoArticle.FindByIDCategory(id)
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatArticles(data, h.repoCategory))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}

func (h *WebHandler) Gallery(c *gin.Context) {
	var response helper.Web
	data, err := h.repoGallery.FindAllGallery()
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatGallerys(data))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}

func (h *WebHandler) KategoriHome(c *gin.Context) {
	var response helper.Web
	filterName := []string{"Aksi", "Inovasi", "Sumber Daya"}
	category, err := h.repoCategory.FindCategoryByName(filterName)
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatCategorys(category))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}

func (h *WebHandler) KontenStatis(c *gin.Context) {
	var response helper.Web
	urlSlug := c.Param("url_slug")
	data, err := h.repoContentStatic.FindByUrLSlug(urlSlug)
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}

	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatContentStatic(data, h.repoCategory))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)

}

func (h *WebHandler) ArtikelDetail(c *gin.Context) {
	var response helper.Web
	urlSlug := c.Param("url_slug")
	data, err := h.repoArticle.FindByUrLSlug(urlSlug)
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}

	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatArticle(data, h.repoCategory))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)

}

func (h *WebHandler) KontenStatisByID(c *gin.Context) {
	var response helper.Web
	id := c.Param("idcategory")
	data, err := h.repoContentStatic.FindByIDCategory(id)
	if err != nil {
		responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatContentStatic(data, h.repoCategory))
	jsonStr, err := json.Marshal(&response)
	dataPad := pkcs7pad.Pad(jsonStr, 16)
	fmt.Println(string(jsonStr))
	text, err := helper.EncryptText(h.configApp, dataPad)
	if err != nil {
		responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
		c.JSON(http.StatusInternalServerError, responseErr)
		return
	}
	fmt.Println(text)
	c.JSON(http.StatusOK, text)
}

func (h *WebHandler) CategoryBy(c *gin.Context) {
	var response helper.Web
	filter := c.Param("filter")

	if filter == "amanat-perubahan-iklim" {
		filterName := []string{"Konvensi", "Amanat", "Komitmen Indonesia"}
		category, err := h.repoCategory.FindCategoryByName(filterName)
		if err != nil {
			responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatCategorys(category))
		jsonStr, err := json.Marshal(&response)
		dataPad := pkcs7pad.Pad(jsonStr, 16)
		fmt.Println(string(jsonStr))
		text, err := helper.EncryptText(h.configApp, dataPad)
		if err != nil {
			responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		fmt.Println(text)
		c.JSON(http.StatusOK, text)
	}

	if filter == "inovasi" {
		filterName := []string{"Inovasi"}
		category, err := h.repoCategory.FindCategoryByName(filterName)
		article, err := h.repoArticle.FindByIDCategory(category[0].ID)
		if err != nil {
			responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatArticles(article, h.repoCategory))
		jsonStr, err := json.Marshal(&response)
		dataPad := pkcs7pad.Pad(jsonStr, 16)
		fmt.Println(string(jsonStr))
		text, err := helper.EncryptText(h.configApp, dataPad)
		if err != nil {
			responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		fmt.Println(text)
		c.JSON(http.StatusOK, text)
	}

	if filter == "hasil-kerjasama" {
		filterName := []string{"Hasil Kerjasama"}
		category, err := h.repoCategory.FindCategoryByName(filterName)
		article, err := h.repoArticle.FindByIDCategory(category[0].ID)
		if err != nil {
			responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatArticles(article, h.repoCategory))
		jsonStr, err := json.Marshal(&response)
		dataPad := pkcs7pad.Pad(jsonStr, 16)
		fmt.Println(string(jsonStr))
		text, err := helper.EncryptText(h.configApp, dataPad)
		if err != nil {
			responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		fmt.Println(text)
		c.JSON(http.StatusOK, text)
	}

	if filter == "mitigasi" {
		filterName := []string{"Mitigasi"}
		category, err := h.repoCategory.FindCategoryByName(filterName)
		article, err := h.repoArticle.FindByIDCategory(category[0].ID)
		if err != nil {
			responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatArticles(article, h.repoCategory))
		jsonStr, err := json.Marshal(&response)
		dataPad := pkcs7pad.Pad(jsonStr, 16)
		fmt.Println(string(jsonStr))
		text, err := helper.EncryptText(h.configApp, dataPad)
		if err != nil {
			responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		fmt.Println(text)
		c.JSON(http.StatusOK, text)
	}

	if filter == "adaptasi" {
		filterName := []string{"Adaptasi"}
		category, err := h.repoCategory.FindCategoryByName(filterName)
		article, err := h.repoArticle.FindByIDCategory(category[0].ID)
		if err != nil {
			responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatArticles(article, h.repoCategory))
		jsonStr, err := json.Marshal(&response)
		dataPad := pkcs7pad.Pad(jsonStr, 16)
		fmt.Println(string(jsonStr))
		text, err := helper.EncryptText(h.configApp, dataPad)
		if err != nil {
			responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		fmt.Println(text)
		c.JSON(http.StatusOK, text)
	}

	if filter == "redd" {
		filterName := []string{"Redd"}
		category, err := h.repoCategory.FindCategoryByName(filterName)
		article, err := h.repoArticle.FindByIDCategory(category[0].ID)
		if err != nil {
			responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatArticles(article, h.repoCategory))
		jsonStr, err := json.Marshal(&response)
		dataPad := pkcs7pad.Pad(jsonStr, 16)
		fmt.Println(string(jsonStr))
		text, err := helper.EncryptText(h.configApp, dataPad)
		if err != nil {
			responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		fmt.Println(text)
		c.JSON(http.StatusOK, text)
	}

	if filter == "proklim" {
		filterName := []string{"Proklim"}
		category, err := h.repoCategory.FindCategoryByName(filterName)
		article, err := h.repoArticle.FindByIDCategory(category[0].ID)
		if err != nil {
			responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatArticles(article, h.repoCategory))
		jsonStr, err := json.Marshal(&response)
		dataPad := pkcs7pad.Pad(jsonStr, 16)
		fmt.Println(string(jsonStr))
		text, err := helper.EncryptText(h.configApp, dataPad)
		if err != nil {
			responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		fmt.Println(text)
		c.JSON(http.StatusOK, text)
	}

	if filter == "perjanjian-paris" {
		filterName := []string{"Perjanjian Paris"}
		category, err := h.repoCategory.FindCategoryByName(filterName)
		article, err := h.repoArticle.FindByIDCategory(category[0].ID)
		if err != nil {
			responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatArticles(article, h.repoCategory))
		jsonStr, err := json.Marshal(&response)
		dataPad := pkcs7pad.Pad(jsonStr, 16)
		fmt.Println(string(jsonStr))
		text, err := helper.EncryptText(h.configApp, dataPad)
		if err != nil {
			responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		fmt.Println(text)
		c.JSON(http.StatusOK, text)
	}

	if filter == "karhutla" {
		filterName := []string{"Karhutla"}
		category, err := h.repoCategory.FindCategoryByName(filterName)
		article, err := h.repoArticle.FindByIDCategory(category[0].ID)
		if err != nil {
			responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatArticles(article, h.repoCategory))
		jsonStr, err := json.Marshal(&response)
		dataPad := pkcs7pad.Pad(jsonStr, 16)
		fmt.Println(string(jsonStr))
		text, err := helper.EncryptText(h.configApp, dataPad)
		if err != nil {
			responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		fmt.Println(text)
		c.JSON(http.StatusOK, text)
	}

	if filter == "srn" {
		filterName := []string{"SRN"}
		category, err := h.repoCategory.FindCategoryByName(filterName)
		article, err := h.repoArticle.FindByIDCategory(category[0].ID)
		if err != nil {
			responseErr := helper.WebResponse("Failed to get data", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		response = helper.WebResponse("success get data", http.StatusOK, "", formatter.FormatArticles(article, h.repoCategory))
		jsonStr, err := json.Marshal(&response)
		dataPad := pkcs7pad.Pad(jsonStr, 16)
		fmt.Println(string(jsonStr))
		text, err := helper.EncryptText(h.configApp, dataPad)
		if err != nil {
			responseErr := helper.WebResponse("Failed to encrypt text", http.StatusInternalServerError, "error", nil)
			c.JSON(http.StatusInternalServerError, responseErr)
			return
		}
		fmt.Println(text)
		c.JSON(http.StatusOK, text)
	}

}

func (h *WebHandler) CreateFeedBack(c *gin.Context) {
	var response helper.Web

	var input input.FeedBackInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.WebResponse("Create FeedBack failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	form := entity.FeedBack{}
	form.Email = html.EscapeString(input.Email)
	form.Message = html.EscapeString(input.Message)
	form.Nama = html.EscapeString(input.Nama)
	form.Subject = html.EscapeString(input.Subject)

	newFeedBack, err := h.repoFeedBack.SaveFeedBack(form)
	if err != nil {
		response := helper.ApiResponse("Create FeedBack failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response = helper.WebResponse("Successfully Create FeedBack", http.StatusOK, "success", formatter.FormatFeedBack(newFeedBack))

	c.JSON(http.StatusOK, response)

}
