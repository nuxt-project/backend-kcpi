package handler

import (
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type feedbackHandler struct {
	service service.FeedBackService
}

func NewFeedBackHandler(service service.FeedBackService) *feedbackHandler {
	return &feedbackHandler{service}
}
func (h *feedbackHandler) GetFeedBack(c *gin.Context) {
	var input input.InputIDFeedBack
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get FeedBack", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	feedbackDetail, err := h.service.FeedBackServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get FeedBack", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail FeedBack", http.StatusOK, "success", formatter.FormatFeedBack(feedbackDetail))
	c.JSON(http.StatusOK, response)
}

func (h *feedbackHandler) GetFeedBacks(c *gin.Context) {
	feedbacks, err := h.service.FeedBackServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get FeedBacks", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of FeedBacks", http.StatusOK, "success", formatter.FormatFeedBacks(feedbacks))
	c.JSON(http.StatusOK, response)
}

func (h *feedbackHandler) CreateFeedBack(c *gin.Context) {
	var input input.FeedBackInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create FeedBack failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	newFeedBack, err := h.service.FeedBackServiceCreate(input)
	if err != nil {
		response := helper.ApiResponse("Create FeedBack failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Create FeedBack", http.StatusOK, "success", formatter.FormatFeedBack(newFeedBack))
	c.JSON(http.StatusOK, response)
}
func (h *feedbackHandler) UpdateFeedBack(c *gin.Context) {
	var inputID input.InputIDFeedBack
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get FeedBacks", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.FeedBackInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update FeedBack failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	updatedFeedBack, err := h.service.FeedBackServiceUpdate(inputID, inputData)
	if err != nil {
		response := helper.ApiResponse("Failed to get FeedBacks", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update FeedBack", http.StatusOK, "success", formatter.FormatFeedBack(updatedFeedBack))
	c.JSON(http.StatusOK, response)
}
func (h *feedbackHandler) DeleteFeedBack(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDFeedBack
	inputID.ID = id
	_, err := h.service.FeedBackServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get FeedBacks", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.FeedBackServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get FeedBacks", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete FeedBack", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
