package handler

import (
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type stackholdersHandler struct {
	service service.StackholdersService
}

func NewStackholdersHandler(service service.StackholdersService) *stackholdersHandler {
	return &stackholdersHandler{service}
}
func (h *stackholdersHandler) GetStackholders(c *gin.Context) {
	var input input.InputIDStackholders
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Stackholders", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	stackholdersDetail, err := h.service.StackholdersServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Stackholders", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail Stackholders", http.StatusOK, "success", formatter.FormatStackholders(stackholdersDetail))
	c.JSON(http.StatusOK, response)
}

func (h *stackholdersHandler) GetStackholderss(c *gin.Context) {
	stackholderss, err := h.service.StackholdersServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get Stackholderss", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of Stackholderss", http.StatusOK, "success", formatter.FormatStackholderss(stackholderss))
	c.JSON(http.StatusOK, response)
}

func (h *stackholdersHandler) CreateStackholders(c *gin.Context) {
	var input input.StackholdersInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Stackholders failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	newStackholders, err := h.service.StackholdersServiceCreate(input)
	if err != nil {
		response := helper.ApiResponse("Create Stackholders failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Create Stackholders", http.StatusOK, "success", formatter.FormatStackholders(newStackholders))
	c.JSON(http.StatusOK, response)
}
func (h *stackholdersHandler) UpdateStackholders(c *gin.Context) {
	var inputID input.InputIDStackholders
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Stackholderss", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.StackholdersInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Stackholders failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	updatedStackholders, err := h.service.StackholdersServiceUpdate(inputID, inputData)
	if err != nil {
		response := helper.ApiResponse("Failed to get Stackholderss", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Stackholders", http.StatusOK, "success", formatter.FormatStackholders(updatedStackholders))
	c.JSON(http.StatusOK, response)
}
func (h *stackholdersHandler) DeleteStackholders(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDStackholders
	inputID.ID = id
	_, err := h.service.StackholdersServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Stackholderss", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.StackholdersServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Stackholderss", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Stackholders", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
