package handler

import (
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type istilahHandler struct {
	service service.IstilahService
}

func NewIstilahHandler(service service.IstilahService) *istilahHandler {
	return &istilahHandler{service}
}
func (h *istilahHandler) GetIstilah(c *gin.Context) {
	var input input.InputIDIstilah
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Istilah", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	istilahDetail, err := h.service.IstilahServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Istilah", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail Istilah", http.StatusOK, "success", formatter.FormatIstilah(istilahDetail))
	c.JSON(http.StatusOK, response)
}

func (h *istilahHandler) GetIstilahs(c *gin.Context) {
	istilahs, err := h.service.IstilahServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get Istilahs", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of Istilahs", http.StatusOK, "success", formatter.FormatIstilahs(istilahs))
	c.JSON(http.StatusOK, response)
}

func (h *istilahHandler) CreateIstilah(c *gin.Context) {
	var input input.IstilahInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Istilah failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	newIstilah, err := h.service.IstilahServiceCreate(input)
	if err != nil {
		response := helper.ApiResponse("Create Istilah failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Create Istilah", http.StatusOK, "success", formatter.FormatIstilah(newIstilah))
	c.JSON(http.StatusOK, response)
}
func (h *istilahHandler) UpdateIstilah(c *gin.Context) {
	var inputID input.InputIDIstilah
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Istilahs", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.IstilahInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Istilah failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	updatedIstilah, err := h.service.IstilahServiceUpdate(inputID, inputData)
	if err != nil {
		response := helper.ApiResponse("Failed to get Istilahs", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Istilah", http.StatusOK, "success", formatter.FormatIstilah(updatedIstilah))
	c.JSON(http.StatusOK, response)
}
func (h *istilahHandler) DeleteIstilah(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDIstilah
	inputID.ID = id
	_, err := h.service.IstilahServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Istilahs", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.IstilahServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Istilahs", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Istilah", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
