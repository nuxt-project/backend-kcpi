package handler

import (
	"backend-apps/configuration"
	"backend-apps/entity"
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/repository"
	"backend-apps/service"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"os"
	"path/filepath"
)

type articleHandler struct {
	service service.ArticleService
	repoCat repository.CategoryRepository
	cfg     *configuration.ConfigApp
}

func NewArticleHandler(
	service service.ArticleService,
	repoCat repository.CategoryRepository,
	cfg *configuration.ConfigApp,
) *articleHandler {
	return &articleHandler{service, repoCat, cfg}
}
func (h *articleHandler) GetArticle(c *gin.Context) {
	var input input.InputIDArticle
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Article", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	articleDetail, err := h.service.ArticleServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Article", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail Article", http.StatusOK, "success", formatter.FormatArticle(articleDetail, h.repoCat))
	c.JSON(http.StatusOK, response)
}

func (h *articleHandler) GetArticles(c *gin.Context) {
	articles, err := h.service.ArticleServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get Articles", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of Articles", http.StatusOK, "success", formatter.FormatArticles(articles, h.repoCat))
	c.JSON(http.StatusOK, response)
}

func (h *articleHandler) CreateArticle(c *gin.Context) {
	var input input.ArticleInput
	err := c.ShouldBind(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Article failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	fileImg, err := c.FormFile("file_img")
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Article failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	ext := filepath.Ext(fileImg.Filename)
	newFileName := "article-" + uuid.New().String() + ext
	err = c.SaveUploadedFile(fileImg, fmt.Sprintf("%s/%s", h.cfg.Pathfile.Image, newFileName))
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Article failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	currentUser := c.MustGet("currentUser").(entity.User)
	input.CreatedBy = currentUser.ID
	input.ModifiedBy = currentUser.ID

	newArticle, err := h.service.ArticleServiceCreate(input, newFileName)
	if err != nil {
		response := helper.ApiResponse("Create Article failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Create Article", http.StatusOK, "success", formatter.FormatArticle(newArticle, h.repoCat))
	c.JSON(http.StatusOK, response)
}
func (h *articleHandler) UpdateArticle(c *gin.Context) {
	var inputID input.InputIDArticle
	var newFileName string
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Articles", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.ArticleInput
	err = c.ShouldBind(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Article failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	fileImg, err := c.FormFile("file_img")
	if fileImg != nil {
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Article failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		ext := filepath.Ext(fileImg.Filename)
		newFileName = "article-" + uuid.New().String() + ext

		//delete file sebelumnya
		dataOld, err := h.service.ArticleServiceGetByID(inputID)
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Article failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		filePath := filepath.Join(h.cfg.Pathfile.Image, dataOld.Image)
		err = os.Remove(filePath)
		if err != nil {
			//errors := helper.FormatValidationError(err)
			//errorMessage := gin.H{"errors": errors}
			//response := helper.ApiResponse("Update Content Static failed", http.StatusBadRequest, "error", errorMessage)
			//c.JSON(http.StatusBadRequest, response)
			//return
		}
		err = c.SaveUploadedFile(fileImg, fmt.Sprintf("%s/%s", h.cfg.Pathfile.Image, newFileName))
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Article failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
	}

	currentUser := c.MustGet("currentUser").(entity.User)
	inputData.ModifiedBy = currentUser.ID

	updatedArticle, err := h.service.ArticleServiceUpdate(inputID, inputData, newFileName)
	if err != nil {
		response := helper.ApiResponse("Failed to get Articles", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Article", http.StatusOK, "success", formatter.FormatArticle(updatedArticle, h.repoCat))
	c.JSON(http.StatusOK, response)
}
func (h *articleHandler) DeleteArticle(c *gin.Context) {
	id := c.Param("id")
	var inputID input.InputIDArticle
	inputID.ID = id
	dataOld, err := h.service.ArticleServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Articles", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.ArticleServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Articles", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	filePath := filepath.Join(h.cfg.Pathfile.Image, dataOld.Image)
	err = os.Remove(filePath)
	if err != nil {
		response := helper.ApiResponse("Failed to remove file", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Article", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
