package handler

import (
	"backend-apps/configuration"
	"backend-apps/entity"
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/repository"
	"backend-apps/service"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"os"
	"path/filepath"
)

type contentstaticHandler struct {
	service service.ContentStaticService
	repoCat repository.CategoryRepository
	cfg     *configuration.ConfigApp
}

func NewContentStaticHandler(service service.ContentStaticService,
	repoCat repository.CategoryRepository,
	cfg *configuration.ConfigApp) *contentstaticHandler {
	return &contentstaticHandler{service, repoCat, cfg}
}
func (h *contentstaticHandler) GetContentStatic(c *gin.Context) {
	var input input.InputIDContentStatic
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get ContentStatic", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	contentstaticDetail, err := h.service.ContentStaticServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get ContentStatic", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail ContentStatic", http.StatusOK, "success", formatter.FormatContentStatic(contentstaticDetail, h.repoCat))
	c.JSON(http.StatusOK, response)
}

func (h *contentstaticHandler) GetContentStatics(c *gin.Context) {
	contentstatics, err := h.service.ContentStaticServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get ContentStatics", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	response := helper.ApiResponse("List of ContentStatics", http.StatusOK, "success", formatter.FormatContentStatics(contentstatics, h.repoCat))
	c.JSON(http.StatusOK, response)
}

func (h *contentstaticHandler) CreateContentStatic(c *gin.Context) {
	var input input.ContentStaticInput
	err := c.ShouldBind(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create ContentStatic failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	fileImg, err := c.FormFile("file_img")
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Content Static failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	ext := filepath.Ext(fileImg.Filename)
	newFileName := "content_static-" + uuid.New().String() + ext
	err = c.SaveUploadedFile(fileImg, fmt.Sprintf("%s/%s", h.cfg.Pathfile.Image, newFileName))
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Content Static failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	currentUser := c.MustGet("currentUser").(entity.User)
	input.CreatedBy = currentUser.ID
	input.ModifiedBy = currentUser.ID
	newContentStatic, err := h.service.ContentStaticServiceCreate(input, newFileName)
	if err != nil {
		response := helper.ApiResponse("Create ContentStatic failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Create ContentStatic", http.StatusOK, "success", formatter.FormatContentStatic(newContentStatic, h.repoCat))
	c.JSON(http.StatusOK, response)
}
func (h *contentstaticHandler) UpdateContentStatic(c *gin.Context) {
	var inputID input.InputIDContentStatic
	var newFileName string
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get ContentStatics", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.ContentStaticInput
	err = c.ShouldBind(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update ContentStatic failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	fileImg, err := c.FormFile("file_img")
	if fileImg != nil {
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Content Static failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		ext := filepath.Ext(fileImg.Filename)
		newFileName = "content_static-" + uuid.New().String() + ext

		//delete file sebelumnya
		dataOld, err := h.service.ContentStaticServiceGetByID(inputID)
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Content Static failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		filePath := filepath.Join(h.cfg.Pathfile.Image, dataOld.Image)
		err = os.Remove(filePath)
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Content Static failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		err = c.SaveUploadedFile(fileImg, fmt.Sprintf("%s/%s", h.cfg.Pathfile.Image, newFileName))
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Content Static failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
	}

	currentUser := c.MustGet("currentUser").(entity.User)
	inputData.ModifiedBy = currentUser.ID

	updatedContentStatic, err := h.service.ContentStaticServiceUpdate(inputID, inputData, newFileName)
	if err != nil {
		response := helper.ApiResponse("Failed to get ContentStatics", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update ContentStatic", http.StatusOK, "success", formatter.FormatContentStatic(updatedContentStatic, h.repoCat))
	c.JSON(http.StatusOK, response)
}
func (h *contentstaticHandler) DeleteContentStatic(c *gin.Context) {
	id := c.Param("id")
	var inputID input.InputIDContentStatic
	inputID.ID = id
	dataOld, err := h.service.ContentStaticServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get ContentStatics", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.ContentStaticServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get ContentStatics", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	filePath := filepath.Join(h.cfg.Pathfile.Image, dataOld.Image)
	err = os.Remove(filePath)
	if err != nil {
		response := helper.ApiResponse("Failed to remove file", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete ContentStatic", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
