package handler

import (
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type surveykepuasanHandler struct {
	service service.SurveyKepuasanService
}

func NewSurveyKepuasanHandler(service service.SurveyKepuasanService) *surveykepuasanHandler {
	return &surveykepuasanHandler{service}
}
func (h *surveykepuasanHandler) GetSurveyKepuasan(c *gin.Context) {
	var input input.InputIDSurveyKepuasan
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get SurveyKepuasan", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	surveykepuasanDetail, err := h.service.SurveyKepuasanServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get SurveyKepuasan", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail SurveyKepuasan", http.StatusOK, "success", formatter.FormatSurveyKepuasan(surveykepuasanDetail))
	c.JSON(http.StatusOK, response)
}

func (h *surveykepuasanHandler) GetSurveyKepuasans(c *gin.Context) {
	startDate := c.Query("start_date")
	endDate := c.Query("end_date")
	surveykepuasans, err := h.service.SurveyKepuasanServiceGetAll(startDate, endDate)
	if err != nil {
		response := helper.ApiResponse("Failed to get SurveyKepuasans", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of SurveyKepuasans", http.StatusOK, "success", formatter.FormatSurveyKepuasans(surveykepuasans))
	c.JSON(http.StatusOK, response)
}

func (h *surveykepuasanHandler) CreateSurveyKepuasan(c *gin.Context) {
	var input input.SurveyKepuasanInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create SurveyKepuasan failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	newSurveyKepuasan, err := h.service.SurveyKepuasanServiceCreate(input)
	if err != nil {
		response := helper.ApiResponse("Create SurveyKepuasan failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Create SurveyKepuasan", http.StatusOK, "success", formatter.FormatSurveyKepuasan(newSurveyKepuasan))
	c.JSON(http.StatusOK, response)
}
func (h *surveykepuasanHandler) UpdateSurveyKepuasan(c *gin.Context) {
	var inputID input.InputIDSurveyKepuasan
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get SurveyKepuasans", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.SurveyKepuasanInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update SurveyKepuasan failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	updatedSurveyKepuasan, err := h.service.SurveyKepuasanServiceUpdate(inputID, inputData)
	if err != nil {
		response := helper.ApiResponse("Failed to get SurveyKepuasans", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update SurveyKepuasan", http.StatusOK, "success", formatter.FormatSurveyKepuasan(updatedSurveyKepuasan))
	c.JSON(http.StatusOK, response)
}
func (h *surveykepuasanHandler) DeleteSurveyKepuasan(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDSurveyKepuasan
	inputID.ID = id
	_, err := h.service.SurveyKepuasanServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get SurveyKepuasans", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.SurveyKepuasanServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get SurveyKepuasans", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete SurveyKepuasan", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
