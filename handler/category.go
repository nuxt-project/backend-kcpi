package handler

import (
	"backend-apps/configuration"
	"backend-apps/entity"
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/service"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"os"
	"path/filepath"
)

type categoryHandler struct {
	service service.CategoryService
	cfg     *configuration.ConfigApp
}

func NewCategoryHandler(service service.CategoryService, cfg *configuration.ConfigApp) *categoryHandler {
	return &categoryHandler{service, cfg}
}
func (h *categoryHandler) GetCategory(c *gin.Context) {
	var input input.InputIDCategory
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Category", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	categoryDetail, err := h.service.CategoryServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Category", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail Category", http.StatusOK, "success", formatter.FormatCategory(categoryDetail))
	c.JSON(http.StatusOK, response)
}

func (h *categoryHandler) GetCategorys(c *gin.Context) {
	categorys, err := h.service.CategoryServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get Categorys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of Categorys", http.StatusOK, "success", formatter.FormatCategorys(categorys))
	c.JSON(http.StatusOK, response)
}

func (h *categoryHandler) GetTypeCategory(c *gin.Context) {
	data, err := h.service.GetDataType()
	if err != nil {
		response := helper.ApiResponse("Failed to get Categorys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of type Categorys", http.StatusOK, "success", data)
	c.JSON(http.StatusOK, response)
}

func (h *categoryHandler) CreateCategory(c *gin.Context) {
	var input input.CategoryInput
	err := c.ShouldBind(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Category failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	fileImg, err := c.FormFile("file_category")
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Category failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	ext := filepath.Ext(fileImg.Filename)
	newFileName := "category-" + uuid.New().String() + ext
	err = c.SaveUploadedFile(fileImg, fmt.Sprintf("%s/%s", h.cfg.Pathfile.Image, newFileName))
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Category failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	currentUser := c.MustGet("currentUser").(entity.User)
	input.CreatedBy = currentUser.ID
	input.ModifiedBy = currentUser.ID
	newCategory, err := h.service.CategoryServiceCreate(input, newFileName)
	if err != nil {
		response := helper.ApiResponse("Create Category failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Create Category", http.StatusOK, "success", formatter.FormatCategory(newCategory))
	c.JSON(http.StatusOK, response)
}
func (h *categoryHandler) UpdateCategory(c *gin.Context) {
	var inputID input.InputIDCategory
	var newFileName string
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Categorys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.CategoryInput
	err = c.ShouldBind(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Category failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	fileImg, err := c.FormFile("file_category")
	if fileImg != nil {
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Category failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		ext := filepath.Ext(fileImg.Filename)
		newFileName = "category-" + uuid.New().String() + ext

		//delete file sebelumnya
		dataOld, err := h.service.CategoryServiceGetByID(inputID)
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Category failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		filePath := filepath.Join(h.cfg.Pathfile.Image, dataOld.Image)
		err = os.Remove(filePath)
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Category failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		err = c.SaveUploadedFile(fileImg, fmt.Sprintf("%s/%s", h.cfg.Pathfile.Image, newFileName))
		if err != nil {
			errors := helper.FormatValidationError(err)
			errorMessage := gin.H{"errors": errors}
			response := helper.ApiResponse("Update Category failed", http.StatusBadRequest, "error", errorMessage)
			c.JSON(http.StatusBadRequest, response)
			return
		}
	}

	currentUser := c.MustGet("currentUser").(entity.User)
	inputData.ModifiedBy = currentUser.ID
	updatedCategory, err := h.service.CategoryServiceUpdate(inputID, inputData, newFileName)
	if err != nil {
		response := helper.ApiResponse("Failed to get Categorys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Category", http.StatusOK, "success", formatter.FormatCategory(updatedCategory))
	c.JSON(http.StatusOK, response)
}
func (h *categoryHandler) DeleteCategory(c *gin.Context) {
	id := c.Param("id")
	var inputID input.InputIDCategory
	inputID.ID = id
	dataOld, err := h.service.CategoryServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Categorys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.CategoryServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Categorys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	filePath := filepath.Join(h.cfg.Pathfile.Image, dataOld.Image)
	err = os.Remove(filePath)
	if err != nil {
		response := helper.ApiResponse("Failed to remove file", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Category", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
