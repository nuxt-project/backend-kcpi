package handler

import (
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type libraryHandler struct {
	service service.LibraryService
}

func NewLibraryHandler(service service.LibraryService) *libraryHandler {
	return &libraryHandler{service}
}
func (h *libraryHandler) GetLibrary(c *gin.Context) {
	var input input.InputIDLibrary
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Library", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	libraryDetail, err := h.service.LibraryServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get Library", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail Library", http.StatusOK, "success", formatter.FormatLibrary(libraryDetail))
	c.JSON(http.StatusOK, response)
}

func (h *libraryHandler) GetLibrarys(c *gin.Context) {
	librarys, err := h.service.LibraryServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get Librarys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of Librarys", http.StatusOK, "success", formatter.FormatLibrarys(librarys))
	c.JSON(http.StatusOK, response)
}

func (h *libraryHandler) CreateLibrary(c *gin.Context) {
	var input input.LibraryInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create Library failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	newLibrary, err := h.service.LibraryServiceCreate(input)
	if err != nil {
		response := helper.ApiResponse("Create Library failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Create Library", http.StatusOK, "success", formatter.FormatLibrary(newLibrary))
	c.JSON(http.StatusOK, response)
}
func (h *libraryHandler) UpdateLibrary(c *gin.Context) {
	var inputID input.InputIDLibrary
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Librarys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.LibraryInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update Library failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	updatedLibrary, err := h.service.LibraryServiceUpdate(inputID, inputData)
	if err != nil {
		response := helper.ApiResponse("Failed to get Librarys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update Library", http.StatusOK, "success", formatter.FormatLibrary(updatedLibrary))
	c.JSON(http.StatusOK, response)
}
func (h *libraryHandler) DeleteLibrary(c *gin.Context) {
	param := c.Param("id")
	id, _ := strconv.Atoi(param)
	var inputID input.InputIDLibrary
	inputID.ID = id
	_, err := h.service.LibraryServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Librarys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.LibraryServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Librarys", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete Library", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
