package handler

import (
	"backend-apps/auth"
	"backend-apps/entity"
	"backend-apps/formatter"
	"backend-apps/helper"
	"backend-apps/input"
	"backend-apps/service"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"net/http"
)

type userHandler struct {
	service     service.UserService
	authService auth.Service
}

func NewUserHandler(service service.UserService, authService auth.Service) *userHandler {
	return &userHandler{service, authService}
}

func (h *userHandler) Login(c *gin.Context) {
	var input input.Login

	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}

		response := helper.ApiResponse("Login failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	loggedinUser, err := h.service.Login(input)

	if err != nil {
		errorMessage := gin.H{"errors": err.Error()}

		response := helper.ApiResponse("Login failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	token, err := h.authService.GenerateToken(loggedinUser.ID)
	if err != nil {
		response := helper.ApiResponse("Login failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	formatter := formatter.FormatUser(loggedinUser, token)

	response := helper.ApiResponse("Successfuly loggedin", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)

}

func (h *userHandler) UpdatePasssword(c *gin.Context) {
	var input input.UpdatePassword

	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}

		response := helper.ApiResponse("Update Password failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	userDetail := c.Value("currentUser").(entity.User)

	err = bcrypt.CompareHashAndPassword(
		[]byte(userDetail.Password),
		[]byte(input.PasswordOld))

	if err != nil {
		response := helper.ApiResponse("Password lama tidak cocok", http.StatusUnprocessableEntity, "error", nil)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	updatePassword, err := h.service.UpdatePassword(input, userDetail)

	if err != nil {
		errorMessage := gin.H{"errors": err.Error()}

		response := helper.ApiResponse("Update Password failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	formatter := formatter.FormatUser(updatePassword, "")

	response := helper.ApiResponse("Successfuly updated password", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)
}

func (h *userHandler) GetUserToken(c *gin.Context) {
	userDetail := c.Value("currentUser").(entity.User)
	response := helper.ApiResponse("Detail User", http.StatusOK, "success", formatter.FormatUser(userDetail, ""))
	c.JSON(http.StatusOK, response)
}

func (h *userHandler) GetUser(c *gin.Context) {
	var input input.InputIDUser
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed to get User", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	userDetail, err := h.service.UserServiceGetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed to get User", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Detail User", http.StatusOK, "success", formatter.FormatUser(userDetail, ""))
	c.JSON(http.StatusOK, response)
}

func (h *userHandler) GetUsers(c *gin.Context) {
	users, err := h.service.UserServiceGetAll()
	if err != nil {
		response := helper.ApiResponse("Failed to get Users", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("List of Users", http.StatusOK, "success", formatter.FormatUsers(users))
	c.JSON(http.StatusOK, response)
}

func (h *userHandler) CreateUser(c *gin.Context) {
	var input input.UserInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Create User failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	input.ID = c.MustGet("request_id").(string)
	newUser, err := h.service.UserServiceCreate(input)
	if err != nil {
		switch err {
		case service.UsernameIsExistError:
			response := helper.ApiResponse(err.Error(), http.StatusBadRequest, "error", nil)
			c.JSON(http.StatusBadRequest, response)
			return
		default:
			response := helper.ApiResponse("Create User failed", http.StatusBadRequest, "error", nil)
			c.JSON(http.StatusBadRequest, response)
			return
		}
	}
	response := helper.ApiResponse("Successfully Create User", http.StatusOK, "success", formatter.FormatUser(newUser, ""))
	c.JSON(http.StatusOK, response)
}
func (h *userHandler) UpdateUser(c *gin.Context) {
	var inputID input.InputIDUser
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Users", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	var inputData input.UserInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.ApiResponse("Update User failed", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	updatedUser, err := h.service.UserServiceUpdate(inputID, inputData)
	if err != nil {
		response := helper.ApiResponse("Failed to get Users", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Update User", http.StatusOK, "success", formatter.FormatUser(updatedUser, ""))
	c.JSON(http.StatusOK, response)
}
func (h *userHandler) DeleteUser(c *gin.Context) {
	id := c.Param("id")
	var inputID input.InputIDUser
	inputID.ID = id
	_, err := h.service.UserServiceGetByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Users", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	_, err = h.service.UserServiceDeleteByID(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed to get Users", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Successfully Delete User", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
}
