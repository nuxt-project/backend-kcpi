package service

import (
	"backend-apps/entity"
	"backend-apps/input"
	"backend-apps/repository"

	"github.com/google/uuid"
)

type FormSurveyService interface {
	FormSurveyServiceGetAll() ([]entity.FormSurvey, error)
	FormSurveyServiceCreate(input input.FormSurveyInput) (entity.FormSurvey, error)
	FormSurveyServiceUpdate(inputID input.InputIDFormSurvey, inputData input.FormSurveyInput) (entity.FormSurvey, error)
}
type formSurveyService struct {
	repository repository.FormSurveyRepository
}

func NewFormSurveyService(repository repository.FormSurveyRepository) *formSurveyService {
	return &formSurveyService{repository}
}
func (s *formSurveyService) FormSurveyServiceCreate(input input.FormSurveyInput) (entity.FormSurvey, error) {
	form := entity.FormSurvey{}
	form.ID = uuid.New().String()
	form.URL = input.URL

	form.CreatedOn = timeNow
	form.ModifiedOn = timeNow
	newFormSurvey, err := s.repository.Save(form)
	if err != nil {
		return newFormSurvey, err
	}
	return newFormSurvey, nil
}

func (s *formSurveyService) FormSurveyServiceUpdate(inputID input.InputIDFormSurvey, inputData input.FormSurveyInput) (entity.FormSurvey, error) {
	form := entity.FormSurvey{}
	form.ID = inputID.ID
	form.URL = inputData.URL
	form.CreatedOn = timeNow
	form.ModifiedOn = timeNow
	newFormSurvey, err := s.repository.Update(form)
	if err != nil {
		return newFormSurvey, err
	}
	return newFormSurvey, nil
}

func (s *formSurveyService) FormSurveyServiceGetAll() ([]entity.FormSurvey, error) {
	forms, err := s.repository.Find()
	if err != nil {
		return forms, err
	}
	return forms, nil
}
