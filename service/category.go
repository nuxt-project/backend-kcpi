package service

import (
	"backend-apps/entity"
	"backend-apps/input"
	"backend-apps/repository"
	"github.com/google/uuid"
)

type CategoryService interface {
	CategoryServiceGetAll() ([]entity.Category, error)
	CategoryServiceGetByID(inputID input.InputIDCategory) (entity.Category, error)
	CategoryServiceCreate(input input.CategoryInput, pathFile string) (entity.Category, error)
	CategoryServiceUpdate(inputID input.InputIDCategory, inputData input.CategoryInput, pathFile string) (entity.Category, error)
	CategoryServiceDeleteByID(inputID input.InputIDCategory) (bool, error)
	GetDataType() ([]entity.TypeCategory, error)
}

type categoryService struct {
	repository repository.CategoryRepository
}

func NewCategoryService(repository repository.CategoryRepository) *categoryService {
	return &categoryService{repository}
}
func (s *categoryService) CategoryServiceCreate(input input.CategoryInput, pathFile string) (entity.Category, error) {
	category := entity.Category{}
	category.ID = uuid.New().String()
	category.Image = pathFile
	category.Category = input.Category
	category.Desc = input.Desc
	category.Type = input.Type
	category.Publish = input.Publish
	category.CreatedBy = input.CreatedBy
	category.ModifiedBy = input.ModifiedBy
	category.ModifiedOn = timeNow
	category.CreatedOn = timeNow
	category.Deleted = input.Deleted
	newCategory, err := s.repository.SaveCategory(category)
	if err != nil {
		return newCategory, err
	}
	return newCategory, nil
}
func (s *categoryService) CategoryServiceUpdate(inputID input.InputIDCategory, inputData input.CategoryInput, pathFile string) (entity.Category, error) {
	category, err := s.repository.FindByIDCategory(inputID.ID)
	if err != nil {
		return category, err
	}
	if pathFile != "" {
		category.Image = pathFile
	}
	category.Category = inputData.Category
	category.Desc = inputData.Desc
	category.Type = inputData.Type
	category.Publish = inputData.Publish
	category.ModifiedBy = inputData.ModifiedBy
	category.ModifiedOn = timeNow
	category.Deleted = inputData.Deleted

	updatedCategory, err := s.repository.UpdateCategory(category)

	if err != nil {
		return updatedCategory, err
	}
	return updatedCategory, nil
}
func (s *categoryService) CategoryServiceGetByID(inputID input.InputIDCategory) (entity.Category, error) {
	category, err := s.repository.FindByIDCategory(inputID.ID)
	if err != nil {
		return category, err
	}
	return category, nil
}
func (s *categoryService) CategoryServiceGetAll() ([]entity.Category, error) {
	categorys, err := s.repository.FindAllCategory()
	if err != nil {
		return categorys, err
	}
	return categorys, nil
}
func (s *categoryService) CategoryServiceDeleteByID(inputID input.InputIDCategory) (bool, error) {
	_, err := s.repository.FindByIDCategory(inputID.ID)
	if err != nil {
		return false, err
	}
	_, err = s.repository.DeleteByIDCategory(inputID.ID)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (s *categoryService) GetDataType() ([]entity.TypeCategory, error) {
	data, err := s.repository.GetDataType()
	if err != nil {
		return data, err
	}
	return data, nil
}

//Generated by Boy at 11 Oktober 2023
