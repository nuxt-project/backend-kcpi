package configuration

import (
	"backend-apps/pkg/logrusx"
	"github.com/spf13/viper"
)

type ConfigApp struct {
	App      App      `mapstructure:"app"`
	Database Database `mapstructure:"database"`
	Pathfile Pathfile `mapstructure:"path-file"`
	Cors     Cors     `mapstructure:"cors"`
	Log      logrusx.Config
}

type App struct {
	Debug        bool   `mapstructure:"debug"`
	Port         string `mapstructure:"port"`
	Timeout      int    `mapstructure:"timeout"`
	SecretKeyJWT string `mapstructure:"secret_key_jwt"`
	SecretKeyAES string `mapstructure:"secret_key_aes"`
}

type Database struct {
	HostName              string `mapstructure:"hostname"`
	Port                  string `mapstructure:"port"`
	Username              string `mapstructure:"username"`
	Password              string `mapstructure:"password"`
	DatabaseName          string `mapstructure:"database_name"`
	MaxIdleConnection     int    `mapstructure:"max_idle_connection"`
	MaxOpenConnection     int    `mapstructure:"max_open_connection"`
	MaxLifetimeConnection int    `mapstructure:"max_lifetime_connection"`
}

type Pathfile struct {
	Image string `mapstructure:"image"`
	Video string `mapstructure:"video"`
}

type Cors struct {
	AllowOrigins []string `mapstructure:"allow_origins"`
}

func LoadConfig(pathFile string) (config ConfigApp, err error) {
	viper.SetConfigType("yaml")
	viper.SetConfigFile(pathFile)

	if err = viper.ReadInConfig(); err != nil {
		return
	}

	if err = viper.Unmarshal(&config); err != nil {
		return
	}

	return
}
