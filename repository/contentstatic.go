package repository

import (
	"backend-apps/entity"
	"gorm.io/gorm"
)

type ContentStaticRepository interface {
	SaveContentStatic(contentstatic entity.ContentStatic) (entity.ContentStatic, error)
	UpdateContentStatic(contentstatic entity.ContentStatic) (entity.ContentStatic, error)
	FindByIDContentStatic(ID string) (entity.ContentStatic, error)
	FindAllContentStatic() ([]entity.ContentStatic, error)
	DeleteByIDContentStatic(ID string) (entity.ContentStatic, error)
	FindByUrLSlug(urlSlug string) (entity.ContentStatic, error)
	FindByIDCategory(IDCategory string) (entity.ContentStatic, error)
}

type contentstaticRepository struct {
	db *gorm.DB
}

func NewContentStaticRepository(db *gorm.DB) *contentstaticRepository {
	return &contentstaticRepository{db}
}

func (r *contentstaticRepository) SaveContentStatic(contentstatic entity.ContentStatic) (entity.ContentStatic, error) {
	err := r.db.Create(&contentstatic).Error
	if err != nil {
		return contentstatic, err
	}
	return contentstatic, nil

}
func (r *contentstaticRepository) FindByIDContentStatic(ID string) (entity.ContentStatic, error) {
	var contentstatic entity.ContentStatic
	err := r.db.Where("id = ? ", ID).Find(&contentstatic).Error
	if err != nil {
		return contentstatic, err
	}
	return contentstatic, nil

}
func (r *contentstaticRepository) UpdateContentStatic(contentstatic entity.ContentStatic) (entity.ContentStatic, error) {
	err := r.db.Save(&contentstatic).Error
	if err != nil {
		return contentstatic, err
	}
	return contentstatic, nil

}
func (r *contentstaticRepository) FindAllContentStatic() ([]entity.ContentStatic, error) {
	var contentstatics []entity.ContentStatic
	err := r.db.Order("created_on desc").Find(&contentstatics).Error
	if err != nil {
		return contentstatics, err
	}
	return contentstatics, nil

}
func (r *contentstaticRepository) DeleteByIDContentStatic(ID string) (entity.ContentStatic, error) {
	var contentstatic entity.ContentStatic
	err := r.db.Where("id = ? ", ID).Delete(&contentstatic).Error
	if err != nil {
		return contentstatic, err
	}
	return contentstatic, nil

}

func (r *contentstaticRepository) FindByUrLSlug(urlSlug string) (entity.ContentStatic, error) {
	var contentstatic entity.ContentStatic
	err := r.db.Where("url_slug = ? ", urlSlug).Find(&contentstatic).Error
	if err != nil {
		return contentstatic, err
	}
	return contentstatic, nil
}

func (r *contentstaticRepository) FindByIDCategory(IDCategory string) (entity.ContentStatic, error) {
	var contentstatic entity.ContentStatic
	err := r.db.Where("id_category = ? ", IDCategory).Find(&contentstatic).Error
	if err != nil {
		return contentstatic, err
	}
	return contentstatic, nil
}

//Generated by Boy at 11 Oktober 2023
