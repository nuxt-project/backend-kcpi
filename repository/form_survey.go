package repository

import (
	"backend-apps/entity"

	"gorm.io/gorm"
)

type FormSurveyRepository interface {
	Save(form entity.FormSurvey) (entity.FormSurvey, error)
	Update(form entity.FormSurvey) (entity.FormSurvey, error)
	Find() ([]entity.FormSurvey, error)
}

type formSurveyRepository struct {
	db *gorm.DB
}

func NewFormSurvetRepository(db *gorm.DB) *formSurveyRepository {
	return &formSurveyRepository{db}
}

func (r *formSurveyRepository) Save(form entity.FormSurvey) (entity.FormSurvey, error) {
	err := r.db.Create(&form).Error
	if err != nil {
		return form, err
	}
	return form, nil
}

func (r *formSurveyRepository) Update(form entity.FormSurvey) (entity.FormSurvey, error) {
	err := r.db.Save(&form).Error
	if err != nil {
		return form, err
	}
	return form, nil
}

func (r *formSurveyRepository) Find() ([]entity.FormSurvey, error) {
	var forms []entity.FormSurvey
	err := r.db.Find(&forms).Error
	if err != nil {
		return forms, err
	}
	return forms, nil
}
