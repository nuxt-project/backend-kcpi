package formatter

import "backend-apps/entity"

type SlideFormatter struct {
	ID         string `json:"id"`
	Url        string `json:"url"`
	Text       string `json:"text"`
	Publish    string `json:"publish"`
	CreatedBy  string `json:"created_by"`
	ModifiedBy string `json:"modified_by"`
	Deleted    int    `json:"deleted"`
}

func FormatSlide(slide entity.Slide) SlideFormatter {
	slideFormatter := SlideFormatter{}
	slideFormatter.ID = slide.ID
	slideFormatter.Url = slide.Url
	slideFormatter.Text = slide.Text
	slideFormatter.Publish = slide.Publish
	slideFormatter.CreatedBy = slide.CreatedBy
	slideFormatter.ModifiedBy = slide.ModifiedBy
	slideFormatter.Deleted = slide.Deleted
	return slideFormatter
}
func FormatSlides(slides []entity.Slide) []SlideFormatter {
	slidesFormatter := []SlideFormatter{}
	for _, slide := range slides {
		slideFormatter := FormatSlide(slide)
		slidesFormatter = append(slidesFormatter, slideFormatter)
	}
	return slidesFormatter
}

//Generated by Boy at 11 Oktober 2023
