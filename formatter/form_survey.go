package formatter

import "backend-apps/entity"

type FormSurveyFormatter struct {
	ID  string `json:"id"`
	Url string `json:"url"`
}

func FormatFormSurvey(form entity.FormSurvey) FormSurveyFormatter {
	formatter := FormSurveyFormatter{}
	formatter.ID = form.ID
	formatter.Url = form.URL

	return formatter
}
func FormatFormSurveys(forms []entity.FormSurvey) []FormSurveyFormatter {
	formatters := []FormSurveyFormatter{}
	for _, form := range forms {
		formatter := FormatFormSurvey(form)
		formatters = append(formatters, formatter)
	}
	return formatters
}
